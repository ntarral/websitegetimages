package com.comp3617.assignment2.mywebsiteloader;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {


    private TextView webPage;
    private String returned;
    private static String[] musicLink;
    private static String[] artLink;
    private static Bitmap[] bitmap;
    private static Bitmap[] musBitmap;

    private static final String URL = "http://www.straight.com/listings/events";
    private static final String BASE_URL = "http://www.straight.com";
    private  URL newUrl;
    private TextView text;

    public static String dataFromAsyncTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        try {
           newUrl = new URL("http://www.straight.com/listings/events");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        new DownloadFilesTask()
                .execute(newUrl);


    }




    private class DownloadFilesTask extends AsyncTask<URL, Void, Event[]> {

        final OkHttpClient client = new OkHttpClient();
        Response response = null;
        protected Event[] doInBackground(URL... urls) {
            long totalSize = 0;
            String htmlPage  = new Downloader().eventsPage(urls[0]);

            String[][] eventsLinks = MainActivity.parseEvents(htmlPage);
            String[] musicsEvents = new Downloader().eventsDetails(eventsLinks[0]);
            String[] artsEvents = new Downloader().eventsDetails(eventsLinks[1]);
            Event[] allmusicEvents = MainActivity.parseEvent(musicsEvents);
            Event[] allartsEvents = MainActivity.parseEvent(artsEvents);

            Log.d("MAIN", "Downloadinging images");
            allmusicEvents = new Downloader().imagesDownload(allmusicEvents);
            for(Event evt : allmusicEvents) {

                try {
                    String absolutePath = saveToInternalStorage(evt.getImage(), evt.getTitle());
                    Log.d("DownloadFilesTask", "file saved in :"+absolutePath);
                    //evt.setImgPath(absolutePath.substring(0, absolutePath.lastIndexOf(File.separator)));

                   // Log.d("DownloadFilesTask", "file saved in :"+saveToInternalStorage(evt.getImage(), evt.getTitle()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            return allmusicEvents;
        }

        private  String saveToInternalStorage(Bitmap bitmapImage, String name) throws IOException {
            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            // Create imageDir
            File mypath=new File(directory,name+".png");

            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(mypath);
                // Use the compress method on the BitMap object to write image to the OutputStream
                bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                fos.close();
            }
            return mypath.getName();
        }




        protected void onPostExecute(Event[]  result) {


            ImageView imageView = (ImageView) findViewById(R.id.imageView);
            ImageView imageView2 = (ImageView) findViewById(R.id.imageView2);
            ImageView imageView3 = (ImageView) findViewById(R.id.imageView3);
            ImageView imageView4 = (ImageView) findViewById(R.id.imageView4);
            ImageView imageView5 = (ImageView) findViewById(R.id.imageView5);
            if (imageView != null) {
                imageView.setImageBitmap(result[0].getImage());
                imageView2.setImageBitmap(result[1].getImage());
                imageView3.setImageBitmap(result[2].getImage());
                imageView4.setImageBitmap(result[3].getImage());
                imageView5.setImageBitmap(result[4].getImage());
            }


        }






    }
    private void displayText(String str) {
        text.setText(str);
    }



    public static class Downloader {





        protected String eventsPage(URL url) {
            final OkHttpClient client = new OkHttpClient();
            Response response = null;
            StringBuilder sb = null;
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            int ch;
            String rep = null;
            sb = new StringBuilder();
            InputStream in = response.body().byteStream();
            try {
                while ((ch = in.read()) != -1)
                    sb.append((char) ch);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return sb.toString();
        }

        protected String[] eventsDetails(String[] urls) {
            String[] allEvents = new String[urls.length];
            final OkHttpClient client = new OkHttpClient();
            Response response = null;
            StringBuilder sb = null;
            int i = 0;
            for (String url : urls) {
                Request request = new Request.Builder()
                        .url(BASE_URL + url)
                        .build();
                try {
                    response = client.newCall(request).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                int ch;
                String rep = null;
                sb = new StringBuilder();
                InputStream in = response.body().byteStream();
                try {
                    while ((ch = in.read()) != -1)
                        sb.append((char) ch);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                allEvents[i] = sb.toString();
                i++;
            }

            return allEvents;
        }

        protected Event[] imagesDownload(Event[] evts) {
            Bitmap[] bitmaps = new Bitmap[evts.length];
            final OkHttpClient client = new OkHttpClient();
            int i = 0;
            for (Event evt : evts) {

                Request request = new Request.Builder()
                        .url("http:"+evt.getImgUrl())
                        .build();

                Response response = null;
                Bitmap mIcon11 = null;
                try {
                    response = client.newCall(request).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (response.isSuccessful()) {
                    try {
                        mIcon11 = BitmapFactory.decodeStream(response.body().byteStream());
                    } catch (Exception e) {
                        Log.e("Error", e.getMessage());
                        e.printStackTrace();
                    }

                }
                // NOTE (nicolas) : do not forgot to save the image in the device with the title name.

                evt.setImage(mIcon11);



            }
            return evts;
        }
    }

    public static Event[] parseEvent(String[] responses ) {

        Event[] evt = new Event[responses.length];
        int i =0;


        for(String response : responses) {

            Document doc = Jsoup.parse(response);
            Elements main = doc.select("main[role=main]");
            String eventsMain = main.get(0).toString();
            //Log.d("MAIN" , "inthe main "+eventsMain);
            Document article = Jsoup.parse(eventsMain);
            Elements title = article.select("h1[class=title]");
            Elements date = article.select("ul[class=event--date]");
            Elements description = article.select("p[itemprop=description]");
            Elements pic = article.select("img[class=movie-poster]");
            Elements loc = article.select("span[itemprop=address]");

            Event event = new Event(title.html(),date.html(),loc.html(),description.html(),pic.attr("src"), null, null, null );
           /* Log.d("MAIN" , "Title "+title.html());
            Log.d("MAIN", "date " + date.html());
            Log.d("MAIN", "Location " + loc.html());
            Log.d("MAIN", "description " + description.html());
            Log.d("MAIN", "pic " + pic.attr("src"));*/

            evt[i] = event;
            i++;
        }



        return evt;


    }



    public static String[][] parseEvents(String response ) {


        Document doc = Jsoup.parse(response);
        //Elements divs = doc.select("div.box--justannounced");

        Elements bucketMus = doc.select("div[class=bucket]");
        Elements bucketEvent = doc.select("div[class=bucket closed]");
        System.out.println(" ---- Musics Events ---- ");
        String eventsDivs = bucketMus.get(0).toString();
        //System.out.println(eventsDivs);
        Document musDocs = Jsoup.parse(eventsDivs);
        Elements musDates = musDocs.select("div[class=event--date]");
        Elements musPics = musDocs.select("img");
        Elements musTitle = musDocs.select("h3[class=title]");
        Elements musLinks = musDocs.select("a");

        String bucketClosed = bucketEvent.get(0).toString();
        Document artDocs = Jsoup.parse(bucketClosed);
        Elements artDates = artDocs.select("div[class=event--date]");
        Elements artPics = artDocs.select("img");
        Elements artTitle = artDocs.select("h3[class=title]");
        Elements artLinks = artDocs.select("a");

        //musicsEvents = new Event[musTitle.size()];
       // artsEvents = new Event[artTitle.size()];
        musicLink = new String[musTitle.size()];
        artLink = new String[artTitle.size()];
        for(int i = 0; i < musTitle.size(); i++) {
          //  Event m = new Event();
           // Log.d("MAIN", musTitle.get(i).html());
           // musicLink[i] = musPics.get(i).attr("data-original");
           // Log.d("MAIN", musPics.get(i).attr("data-original"));
           // Log.d("MAIN", musLinks.get(i).attr("href"));
            musicLink[i] = musLinks.get(i).attr("href");
           // Log.d("MAIN", musDates.get(i).html());
           //musicsEvents[i] = m;
        }


        System.out.println(" ---- Events Events ---- ");



        for(int i = 0; i < artTitle.size(); i++) {
            //Event ma = new Event();
           // Log.d("MAIN", artTitle.get(i).html());
          //  Log.d("MAIN", artPics.get(i).attr("data-original"));
          //  Log.d("MAIN", artLinks.get(i).attr("href"));
            artLink[i] = artLinks.get(i).attr("href");
           // artLink[i] = artPics.get(i).attr("data-original");
          //  Log.d("MAIN", artDates.get(i).html());
            //artsEvents[i]= ma;

        }

        String[][] eventsLinks = new String[][] {musicLink,artLink};
        return eventsLinks;


    }




    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap[]> {
        ImageView bmImage;



        protected Bitmap[] doInBackground(String... urls) {
            Thread.currentThread().setName("Foo (AsyncTask)");
            Response[] responses = new Response[urls.length];
            Bitmap[] bitmaps = new Bitmap[urls.length];
            Response response =null;
            final OkHttpClient client = new OkHttpClient();
            Log.d("MAIN", "image loaded");
            int i =0;
            for (String url : urls) {
                Request request = new Request.Builder()
                        .url("http:"+url)
                        .build();


                Bitmap mIcon11 = null;
                try {
                    response = client.newCall(request).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (response.isSuccessful()) {
                    try {
                        mIcon11 = BitmapFactory.decodeStream(response.body().byteStream());
                    } catch (Exception e) {
                        Log.e("Error", e.getMessage());
                        e.printStackTrace();
                    }
                    bitmaps[i] = mIcon11;
                    i++;

                }
            }
            Log.d("MAIN", "on thread "+Thread.currentThread().getName());
            return bitmaps;

        }

        protected void onPostExecute(Bitmap[] result) {
            MainActivity.bitmap = result;
            ImageView imageView = (ImageView) findViewById(R.id.imageView);
            ImageView imageView2 = (ImageView) findViewById(R.id.imageView2);
            ImageView imageView3 = (ImageView) findViewById(R.id.imageView3);
            ImageView imageView4 = (ImageView) findViewById(R.id.imageView4);
            ImageView imageView5 = (ImageView) findViewById(R.id.imageView5);
            if (imageView != null) {
                imageView.setImageBitmap(result[0]);
                imageView2.setImageBitmap(result[1]);
                imageView3.setImageBitmap(result[2]);
                imageView4.setImageBitmap(result[3]);
                imageView5.setImageBitmap(result[4]);
            }

        }
    }



}


