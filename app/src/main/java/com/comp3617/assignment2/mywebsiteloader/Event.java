package com.comp3617.assignment2.mywebsiteloader;

import android.graphics.Bitmap;

public class Event {

	private String title;
	private String date;
	private String loc;
	private String text;
	private String imgUrl;
	private Bitmap image;
	private String link;
	private String imgPath;


	public Event(String title, String date, String loc, String text, String imgUrl, Bitmap image, String link, String imgPath) {
		super();
		this.title = title;
		this.date = date;
		this.loc = loc;
		this.text = text;
		this.imgUrl = imgUrl;
		this.image = image;
		this.link = link;
		this.imgPath = imgPath;

	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getLoc() {
		return loc;
	}

	public void setLoc(String loc) {
		this.loc = loc;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public Bitmap getImage() {
		return image;
	}

	public void setImage(Bitmap image) {
		this.image = image;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	@Override
	public String toString() {
		return "Event{" +
				"title='" + title + '\'' +
				", date='" + date + '\'' +
				", loc='" + loc + '\'' +
				", text='" + text + '\'' +
				", imgUrl='" + imgUrl + '\'' +
				", image=" + image +
				", link='" + link + '\'' +
				'}';
	}
}